import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import LoginPage from "./LoginPage";

class App extends Component {
  constructor(props){
    super();
    this.state= {
      count:0
    }
  }
  
  handleIncrement= () => {
    // let { count } =this.state
    this.setState({
      count:this.state.count+1
    })
    document.getElementById("counter").innerHTML = this.state.count;
  }
  
  handleDecrement= () => {
    // let { count } =this.state
    this.setState({
      count:this.state.count-1
    })
    document.getElementById("counter").innerHTML = this.state.count;
  }


  render() {
    
    return (
      <div className="App">
       <div className="container">
       <button className="btn button" onClick={this.handleIncrement}>increment</button>
       <p>Count is: <label id="counter"></label></p>
       <button className="btn button" onClick={this.handleDecrement}>Decrement</button>
       </div>
       <LoginPage></LoginPage>
      </div>
    );
  }
}

export default App;
