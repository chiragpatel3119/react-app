import React, { Component } from 'react';
import './LoginPage.css';
import Route from 'react-router-dom';
import ReactDom from "react-dom";

const API = 'https://hn.algolia.com/api/v1/search?query=';
const DEFAULT_QUERY = 'a';

class LoginPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      username:'',
      password:'',
      message:'',
      data:null,
      hits:[],
      search:'',
    };

    this.handlePassChange = this.handlePassChange.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissError = this.dismissError.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
   
  }//Class

  componentDidMount(){
    fetch(API + DEFAULT_QUERY)
    .then(Response => Response.json())
    .then(data => this.setState({hits:data.hits}));
  }

  handleUserChange(evt){
    this.setState({
      username: evt.target.value,
    });
  };//handleUserChange

  handlePassChange(evt){
    this.setState({
      password: evt.target.value,
    });
  }//handlePassChange

  dismissError(){
    this.setState({message:''});
  }

  handleSubmit(evt){
    evt.preventDefault();

    if(!this.state.username){
      return this.setState({ message: 'username is required!'});
    }
    if(!this.state.password){
      return this.setState({ message: 'Password is required!'});
    }

    if(this.state.username=="chirag" && this.state.password=="123"){
      return this.setState({message:'Login Successful!'});

    }

    return this.setState({message:'Wrong Credential.'});
    
  };

  handleSearch(evt){
    this.setState({
      search:evt.target.value,
    });
    console.log(this.state.search);
  }

  // componentWillMount(){}
  // componentDidMount(){}
  // componentWillUnmount(){}

  // componentWillReceiveProps(){}
  // shouldComponentUpdate(){}
  // componentWillUpdate(){}
  // componentDidUpdate(){}

  render() {
    const {hits} = this.state;
    return (
      <div className="Login">
        <form className="login-form" onSubmit={this.handleSubmit}>
          {
            this.state.message &&
            <h3 data-test="message" onChange={this.dismissError}>
                <button onClick={this.dismissError}>X</button>
                {this.state.message}
            </h3>
          }
          <label>User Name</label>
          <input type="text" data-test="username" value={this.state.username} onChange={this.handleUserChange}></input>
          <label>Password</label>
          <input type="password" data-test="password" value={this.state.password} onChange={this.handlePassChange}></input>
          <input type="submit" value="Log In" data-test="submit"></input>
        </form>


        <h3>Search data:</h3>
        <input type="text" value={this.state.search} onChange={this.handleSearch}></input>

        <div className="fetched__data">
          <ul>
              {hits.map(hit => 
                <li key={hit.objectID}>
                  <a href={hit.url}>{hit.title}</a>
                </li>
                )}
          </ul>
        </div>
      </div>
    );
  }
}

export default LoginPage;